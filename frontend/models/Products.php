<?php

namespace frontend\models;

use Yii;
/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property int|null $category
 * @property string $image
 * @property string $description
 * @property string|null $image_path
 *
 * @property Categorys $category0
 */
class Products extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name','required', 'message' => 'This field is required. '],
            [['name'],'unique'],
            ['description','required', 'message' => 'This field is required.'],
            ['category','required', 'message' => 'This field is required.'],
            // [['image'], 'required'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png'],

            ['image', 'required', 'message' => 'This field is required.', 'on' => self::SCENARIO_CREATE],
            [['category'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => Categorys::className(), 'targetAttribute' => ['category' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category' => 'Category',
            'image' => 'Image',
            'description' => 'Description',
            'image_path' => 'Image Path',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['name', 'category', 'image', 'description'];

        return $scenarios;
    }

    /**
     * Gets query for [[Category0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(Categorys::className(), ['id' => 'category']);
    }
}
