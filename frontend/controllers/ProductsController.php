<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\Products;
use frontend\models\Categorys;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\UploadedFile;
use dosamigos\ckeditor\CKEditor;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProductsController extends \yii\web\Controller
{
    public function actionHome()
    {

        $products = Products::find()->with('category0')->all();
        // echo "<pre>";print_r($products);exit;
        return $this->render('home', ['product' => $products,]);
        // return $this->render('home');

    }

    public function actionCreate()
    {
        $category = Categorys::find()->asArray()->all();
        $categories = ArrayHelper::map($category, 'id', 'name');
        //  echo "<pre>";print_r($categories);exit;
        $products = new Products;
        $products->scenario = 'create';
        $formData = Yii::$app->request->post();
        if (Yii::$app->request->isAjax) {
            $products->load($formData);
            // echo '<pre>'; print_r($products->validated());exit;
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($products, ['name','image']);   
        }
        if (Yii::$app->request->isPost) {
        // echo "<pre>";print_r($formData);exit;
            if ($products->load($formData)) {
                // echo "<pre>";print_r($products);exit;
                // if($products->validate()) {
                    $products->image = UploadedFile::getInstance($products, 'image');
                    $fileName = time() . '.' . $products->image->extension;
                    $products->image->saveAs('uploads/' . $fileName);
                    $products->image = $fileName;
                    // $products->save();

                    if ($products->save(false)) {
                        Yii::$app->getSession()->setFlash('message', 'Post published seccessfully');
                        return $this->redirect(['home']);
                    } else {
                        Yii::$app->getSession()->setFlash('message', 'Failed to Post.');
                    }
                // } 
            }
        }

        return $this->render('create', ['product' => $products, 'categories' => $categories]);
    }


    public function actionView($id)
    {
        $products = Products::findOne($id);

        return $this->render('view', ['product' => $products]);
    }

    public function actionUpdate($id)
    {
        $category = Categorys::find()->asArray()->all();
        $categories = ArrayHelper::map($category, 'id', 'name');

        $products = Products::findOne($id);
        $old_image = $products->image;
        if ($products->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($products, 'image');
            if ($image) {
                $fileName = time() . '.' . $image->extension;
                $image->saveAs('uploads/' . $fileName);
                $products->image = $fileName;
            } else {
                $products->image = $old_image;
            }
            if ($products->save(false)) {
                if($image && file_exists(Yii::$app->basePath . '/web/uploads/' . $old_image)){
                    unlink(Yii::$app->basePath . '/web/uploads/' . $old_image);
                }
                // $this->findModel($id)->delete();
            
                Yii::$app->getsession()->setFlash('message', 'Post Updated seccessfully');
                return $this->redirect('home');
            } else {
                Yii::$app->getSession()->setFlash('message', 'Failed to Post.');
            }
        }
        return $this->render('update', ['product' => $products, 'categories' => $categories]);
    }
    public function actionDelete($id)
    {
        $products = Products::findOne($id)->delete();
        if ($products) {
            yii::$app->getSession()->setFlash('message', 'Post Deleted Successfully.');
            return $this->redirect(['home']);
        }   
    }
}

