<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>
<?php if (yii::$app->session->hasFlash('message')); ?>
        <div class="alert-success">
            <?php echo yii::$app->session->getFlash('message'); ?>
        </div>

    <?= $form->field($userform, 'name') ?>

    <?= $form->field($userform, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>