<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="text-center">
<!--        this is for testion -->
        <?php if (yii::$app->session->hasFlash('message')); ?>
        <div class="alert-success">
            <?php echo yii::$app->session->getFlash('message'); ?>
        </div>
    </div>
    <h1 class="text-center padding-bottom" style="color:Blue">Yii2 Crude Application Tutorial.</h1>
</div>
<div class="row">
    <span style="margin-bottom: 20px"><?= Html::a('Create', ['/products/create'], ['class' => 'btn btn-primary']) ?></span>
</div>
<div class="body-content">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
                <th scope="col" style="text-align: center;">Action</th>

            </tr>
        </thead>
        <tbody>
            <?php if (count($product) > 0) : ?>
                <?php foreach ($product as $product) : ?>
                    <tr class="table-active">
                        <th scope="row"><?php echo $product->id; ?></th>
                        <td><?php echo $product->name; ?></td>
                        <td><?php echo $product->category0['name']; ?></td>
                        <td><?php echo $product->description; ?></td>
                        <td><img width="60" src="<?php echo Yii::getAlias('@web') . '/uploads/' . $product->image; ?>"></td>
                        <td style="text-align: center;"> 
                            <span> <?= Html::a('View', ['view', 'id' => $product->id], ['class' => 'btn btn-primary']) ?></span>
                            <span> <?= Html::a('Update', ['update', 'id' => $product->id], ['class' => 'btn btn-secondary']) ?></span>
                            <span> <?= Html::a('Delete', ['delete', 'id' => $product->id], [
                                        'class' => 'btn btn-danger',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]) ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else :   ?>
                <tr>
                    <td> No Records Found!</td>
                </tr>
            <?php endif; ?>
        </tbody>
        </tbody>
    </table>
    <div class="row">
    </div>
</div>
</div>