<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h1>View Post</h1>

    <div class="body-content">
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?php echo $product->name; ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?php echo $product->category0['name']; ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?php echo $product->description; ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
            <img width="60" src="<?php echo Yii::getAlias('@web').'/uploads/'.$product->image; ?>">
            </li>
        </ul>
        <br>
        <div>
        <a href=<?php echo Yii::$app->request->referrer;?> class="btn btn-primary"> Go Back </a>
        </div>
    </div>
</div>

