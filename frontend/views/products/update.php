<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h1>Update Post</h1>

    <div class="body-content">
     <?php 
        $form = ActiveForm::begin()?>
        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                   <?= $form->field($product, 'name');?>
              </div>
           </div>
        </div>

        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
               <?= $form->field($product, 'category')->dropDownList($categories, ['prompt' => 'Select']);?>
            </div>
           </div>
        </div>

        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                   <?= $form->field($product, 'description')->widget(CKEditor::className(),[
                      'options' => ['rows' => 2],
                      'preset' => 'advance',
                   ]) ?>
            </div>
           </div>
        </div> 

        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
               <img width="60" src="<?php echo Yii::getAlias('@web').'/uploads/'.$product->image; ?>">
                   <?= $form->field($product, 'image')->fileInput() ?>

              </div>
           </div>
        </div>
       

        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                    <?php echo Html::submitButton('Update Post', array('name' => 'button1', 'class' => 'btn btn-primary')); ?>
                     <? echo '&nbsp;&nbsp;&nbsp;'; ?>
                     <a href=<?php echo Yii::$app->request->referrer;?> class="btn btn-primary"> Go Back </a>
            </div>                  
           </div>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

