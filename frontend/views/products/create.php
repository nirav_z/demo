<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;



/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h1>Create Post</h1>

    <div class="body-content">
     <?php 
        $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']])?>
        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                   <?= $form->field($product, 'name', ['enableAjaxValidation' => true]);?>
              </div>
           </div>
        </div>
        
        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                   <?= $form->field($product, 'category')->dropDownList($categories, ['prompt' => 'Select']);?>
            </div>
           </div>
        </div>

        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                   <?= $form->field($product, 'description')->widget(CKEditor::className(),[
                      'clientOptions' => ['height' => 150],
                      'preset' => 'advance',
                   ]) ?>
            </div>
           </div>
        </div> 

        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
               <?= $form->field($product, 'image')->fileInput() ?>
              </div>
           </div>
        </div>


        <div class="row">   
           <div class="form-group">
               <div class="col-lg-6" style="width: 1000px;">
                    <?php echo Html::submitButton('Create Post', array('name' => 'button1', 'class' => 'btn btn-primary')); ?>
                     <? echo '&nbsp;&nbsp;&nbsp;'; ?>
                     <a href=<?php echo Url::to(['products/home']) ?> class="btn btn-primary"> Go Back </a>
            </div>                  
           </div>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

