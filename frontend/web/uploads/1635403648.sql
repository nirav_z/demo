-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 17, 2021 at 06:07 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--
CREATE DATABASE IF NOT EXISTS `crud` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crud`;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `sid` int(10) NOT NULL AUTO_INCREMENT,
  `sname` varchar(30) NOT NULL,
  `saddress` varchar(100) NOT NULL,
  `sclass` int(10) NOT NULL,
  `sphone` varchar(10) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentclass`
--

DROP TABLE IF EXISTS `studentclass`;
CREATE TABLE IF NOT EXISTS `studentclass` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(15) NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentclass`
--

INSERT INTO `studentclass` (`cid`, `cname`) VALUES
(1, 'BCA'),
(2, 'Btech'),
(3, 'BSC'),
(4, 'B.COM');
--
-- Database: `crud_demo`
--
CREATE DATABASE IF NOT EXISTS `crud_demo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crud_demo`;

-- --------------------------------------------------------

--
-- Table structure for table `crud_table`
--

DROP TABLE IF EXISTS `crud_table`;
CREATE TABLE IF NOT EXISTS `crud_table` (
  `id` int(100) NOT NULL,
  `user name` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crud_table`
--

INSERT INTO `crud_table` (`id`, `user name`, `password`) VALUES
(1, 'neel', '5'),
(0, 'radhe', '1'),
(3, 'sagar', '2'),
(2, 'nirav', '3');
--
-- Database: `demo_d`
--
CREATE DATABASE IF NOT EXISTS `demo_d` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `demo_d`;
--
-- Database: `my_test`
--
CREATE DATABASE IF NOT EXISTS `my_test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `my_test`;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `f_name` varchar(254) NOT NULL,
  `l_name` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `contact` varchar(254) DEFAULT NULL,
  `address` text NOT NULL,
  `city` varchar(254) NOT NULL,
  `state` varchar(254) NOT NULL,
  `date_of_birth` datetime(6) NOT NULL,
  `marital_status` varchar(254) NOT NULL,
  `gender` varchar(254) NOT NULL,
  `password` varchar(254) NOT NULL,
  `confirm_password` varchar(254) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`f_name`, `l_name`, `email`, `contact`, `address`, `city`, `state`, `date_of_birth`, `marital_status`, `gender`, `password`, `confirm_password`, `id`) VALUES
('sagar', 'Savaliya', 's@gmail.com', '99434567432', 'c.222.esdfhfkerteh', 'rajkot', 'Gujarat', '1998-08-17 00:00:00.000000', 'Single', 'male', 'dgdfgf', 'dfgfdg', 62),
('parth', 'mehata', 'parth@33gmail.com', '9903454545', 'd.323,fgfkgfdkjgfdjg', 'jetpur', 'Chattisgah', '1999-08-26 00:00:00.000000', 'Single', 'male', 'pdsfdgj', 'pdsfdg', 63),
('parth', 'mehata', 'parth@33gmail.com', '9903454545', 'd.323,fgfkgfdkjgfdjg', 'jetpur', 'Chattisgah', '1999-08-26 00:00:00.000000', 'Single', 'male', '', '', 66),
('parth', 'mehata', 'parth@33gmail.com', '9903454545', 'd.323,fgfkgfdkjgfdjg', 'jetpur', 'Chattisgah', '1999-08-26 00:00:00.000000', 'Single', 'male', '', '', 68);
--
-- Database: `newcrud`
--
CREATE DATABASE IF NOT EXISTS `newcrud` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `newcrud`;

-- --------------------------------------------------------

--
-- Table structure for table `new_table`
--

DROP TABLE IF EXISTS `new_table`;
CREATE TABLE IF NOT EXISTS `new_table` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `user name` varchar(10) NOT NULL,
  `password` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_table`
--

INSERT INTO `new_table` (`id`, `user name`, `password`) VALUES
(1, 'Sagar', 1212),
(2, 'nirav', 2121);
--
-- Database: `std`
--
CREATE DATABASE IF NOT EXISTS `std` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `std`;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `age` int(50) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `name`, `age`, `address`) VALUES
(1, 'nirav', 22, 'afdggxcxxgxfcb'),
(1, 'nirav', 22, 'afdggxcxxgxfcb'),
(2, 'Radhe', 26, 'Afdfsjsdsdss');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
