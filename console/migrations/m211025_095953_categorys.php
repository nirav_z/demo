<?php

use yii\db\Migration;

/**
 * Class m211025_095953_categorys
 */
class m211025_095953_categorys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categorys', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('categorys');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211025_095953_categorys cannot be reverted.\n";

        return false;
    }
    */
}
